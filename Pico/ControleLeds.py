'''
Código para acender dois leds de forma alternada entre si
'''

import machine
import utime


ledVermelho = machine.Pin(15, machine.Pin.OUT)
ledVerde = machine.Pin(11, machine.Pin.OUT)

print("START")

def RED():
    ledVermelho.value(1)
    ledVerde.value(0)
    
    
def GREEN():
    ledVerde.value(1)
    ledVermelho(0)
    

while True:
    GREEN()
    utime.sleep(2)
    RED()
    utime.sleep(2)