"""
Programa para buscar e identificar os aparelhos Bluetooth BLE nas proximidades.A biblioteca blietooth precisa estar instalada na RPI


Sequência de comandos para a instalação da biblioteca bluetooth na RPI:
sudo apt-get update
sudo apt-get install python3-pip python3-dev ipython
sudo apt-get install bluetooth bluez libbluetooth-dev
sudo python3 -m pip install pybluez


referencias:
- https://www.filipeflop.com/blog/automacao-basica-bluetooth-ble-na-raspberry-pi/
- https://raspberrypi.stackexchange.com/questions/45246/bluetooth-import-for-python-raspberry-pi-3

"""

import time
import bluetooth

def search():
    devices = bluetooth.discover_devices(duration = 20, lookup_names = True)
    # cada varredura tem duração de 20s
    return devices

while True:
    print("[SCAN] Scan BLE sendo realizado... aguarde...")
    results = search()
    
    if(results != None):
        for addr, name in results:
            print("Dispositivo encontrado: {} - {}".format(addr, name))
            time.sleep(1)
            #print(" ")
            
    print("[SCAN] Fim do scan BLE.")
    print(" ")


